/*******************
Google Tag Manager : Events & DataLayer

/!\ This is the REVEAL.JS version. If you don't use reveal please use the other .js /!\

Author: LOPEZ Pierre-Yves
Version: 1.0.0
Company: Aptus Health
********************/

jQuery(window).load(function() {
	var timeout;
	//On reveal ready.
	pushDataLayer();
	dataLayer.push({'event': 'gtmDocumentReady'});

	jQuery(window).on('slideChange', function (e) {
		clearTimeout(timeout);

		//Wait a bit before push the datalayer - 
		//(example: when the user click on the pathNav)
		timeout = setTimeout(function(){
			pushDataLayer();
			dataLayer.push({'event': 'gtmSlideChange'});
		}, 500);
		
	});

	/*******
	Eventlistener : onclick
	********/
	jQuery('*[class*="gtmClick"]').on('click', function(){

		var classes = jQuery(this).attr('class').split(' ');
		var dataGtm = jQuery(this).data('gtm');
		var eventName,
			eventLabel = '';

		jQuery.each(classes, function(i, val){
			if(val.indexOf("gtmClick") != -1){
				eventName = val;
			}
		});

		if(dataGtm){ eventLabel = dataGtm;}

		pushDataLayer();
		dataLayer.push({
			'event': eventName,
			'eventLabel': eventLabel
		});
	});

	/*******
	Eventlistener : swipe
	********/
	jQuery('*[class*="gtmSwipe"]').on('swipe', function(){

		var classes = jQuery(this).attr('class').split(' ');
		var dataGtm = jQuery(this).data('gtm');
		var eventName,
			eventLabel = '';

		jQuery.each(classes, function(i, val){
			if(val.indexOf("gtmSwipe") != -1){
				eventName = val;
			}
		});
		if(dataGtm){ eventLabel = dataGtm;}

		pushDataLayer();
		dataLayer.push({
			'event': eventName,
			'eventLabel': eventLabel
		});
	});

	/*******
	Eventlistener : Quiz
	********/
	jQuery('*[class*="gtmQuiz"]').on('click', function(){

		var classes = jQuery(this).attr('class').split(' ');
		var quiz = jQuery(this).parent().attr('id');
		var dataGtm = jQuery(this).data('gtm');
		var eventName,
			eventLabel = '';

		jQuery.each(classes, function(i, val){
			if(val.indexOf("gtmQuiz") != -1){
				eventName = val;
			}
		});
		if(dataGtm){ eventLabel = quiz + ' - ' + dataGtm;}

		pushDataLayer();
		dataLayer.push({
			'event': eventName,
			'eventLabel': eventLabel
		});
	});

	/*******
	Eventlistener : keydown
	********/
	var arrayKeyCode = [
		{
			name: 'NavigateKeyPrev',
			code: 37
		},
		{
			name: 'NavigateKeyNext',
			code: 39
		}
	];

	jQuery('body').keydown(function(e){
		jQuery.each(arrayKeyCode, function(i, val){
			if(e.keyCode == val.code){
				pushDataLayer();
				dataLayer.push({'event': 'gtmKeydownNavigate' });
			}
		});
	});

	/****************
	--- FUNCTIONS ---
	****************/

	function pushDataLayer(type, elem){

		var currentSlide = '';
		var slide_active = '';
		var screen_name = '';
		var screen_id = '';
		var screen_section = '';
		var screen_number = '';
		var screen_metadata_1 = '';
		var screen_metadata_2 = '';
		var screen_metadata_3 = '';
		var screen_metadata_4 = '';
		var screen_metadata_5 = '';

		slide_active = jQuery('section.active');

		screen_name = slide_active.data('screen-name');
		screen_id = slide_active.data('screen-id');
		screen_section = slide_active.data('screen-section');
		screen_number = slide_active.data('screen-number');
		screen_metadata_1 = slide_active.data('screenmetadata1');
		screen_metadata_2 = slide_active.data('screenmetadata2');
		screen_metadata_3 = slide_active.data('screenmetadata3');
		screen_metadata_4 = slide_active.data('screenmetadata4');
		screen_metadata_5 = slide_active.data('screenmetadata5');
		
		dataLayer.push({'screenName': screen_name });
		dataLayer.push({'screenSection': screen_section });
		dataLayer.push({'screenID': screen_id });
		dataLayer.push({'screenNumber': screen_number });
		dataLayer.push({'screenMetadata1': screen_metadata_1 });
		dataLayer.push({'screenMetadata2': screen_metadata_2 });
		dataLayer.push({'screenMetadata3': screen_metadata_3 });
		dataLayer.push({'screenMetadata4': screen_metadata_4 });
		dataLayer.push({'screenMetadata5': screen_metadata_5 });

	}
});
