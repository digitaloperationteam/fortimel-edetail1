
<header id="header" class="header container" style="position:fixed; top:0; z-index:100; width:100%;">
    <nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
		<h1 class="text-muted pull-left logo"><img width="140" src="<?php print base_path() . path_to_theme(); ?>/dist/images/<?php echo $language->language; ?>/logoFortimel.png" alt=""></h1>
		<ul id="menu" class="nav nav-pills pull-right">
          	<li data-menuanchor="accueil" class="active"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#accueil" data-origin="topmenu">Accueil</a></li>
          	<li data-menuanchor="quiz"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#quiz" data-origin="topmenu">Quiz</a></li>
          	<li data-menuanchor="situations-a-risque"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#situations-a-risque" data-origin="topmenu">Situations &#224; risque</a></li>
          	<li data-menuanchor="diagnostic"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#diagnostic" data-origin="topmenu">Diagnostic</a></li>
          	<li data-menuanchor="efficacite-clinique"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#efficacite-clinique" data-origin="topmenu">Efficacit&#233; clinique</a></li>
          	<li data-menuanchor="produit"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#produit" data-origin="topmenu">Produit</a></li>
          	<li data-menuanchor="gamme"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#gamme" data-origin="topmenu">Gamme</a></li>
          	<li data-menuanchor="observance"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#observance" data-origin="topmenu">Observance</a></li>
          	<li data-menuanchor="posologie"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#posologie" data-origin="topmenu">Posologie</a></li>
          	<li class="visible-xs visible-sm"><a class=" gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a></li>
          	<li data-menuanchor="end"><a class="gtmClickNavigate" data-gtm="primary-nav" href="#end">Conclusion</a></li>
        </ul>
        <button type="button" class="navbar-toggle open" >
	    <span class="bar1 icon-bar"></span>
	    <span class="bar2 icon-bar"></span>
	    <span class="bar3 icon-bar"></span>
	  </button>
	</nav>
	<div class="navbar navbar-default navbar-fixed-top">
	  <button type="button" class="navbar-toggle">
	    <span class="bar1 icon-bar"></span>
	    <span class="bar2 icon-bar"></span>
	    <span class="bar3 icon-bar"></span>
	  </button>
	  <h1 class="text-muted  pull-left logo"><img width="100" src="<?php print base_path() . path_to_theme(); ?>/dist/images/<?php echo $language->language; ?>/logoFortimel.png" alt=""></h1>
	</div>
</header>
<div id="fullpage">
	<?php echo $slides;	?>
</div>


		<!--- POPINS -->
		<div class="lightbox" id="popinQuiz">
			<div class="closePopin">
				<span>Fermer</span>
			</div>
			<div class="container">
				<h2>Vos patients &#226;g&#233;s</h2>
				<h3 class="text-uppercase">
		    	  	souffrent-ils de d&#233;nutrition&nbsp;?
	    	  	</h3>
		    	<h3 class="mb4">
		    	  	Testez vos connaissances&nbsp;: ces affirmations sont-elles vraies ou fausses&nbsp;?
	    	  	</h3>
			
				  <div class="row mb2 hidden-xs">
					  <div class="col-xs-12 col-sm-4">
					  	<p>
					  		<strong>La pr&#233;valence de la d&#233;nutrition prot&#233;ino-&#233;nerg&#233;tique est de 2 &#224; 5&nbsp;% chez les personnes &#226;g&#233;es vivant &#224; domicile.</strong>
					  	</p>
					  </div>
					  <div class="col-xs-12 col-sm-4">
					  	<p>
					  		<strong>La d&#233;nutrition peut entra&#238;ner ou aggraver la fragilit&#233; physique.</strong>
					  	</p>
					  </div>
					  <div class="col-xs-12 col-sm-4">
					  	<p><strong>L&#8217;isolement social est une situation &#224; risque de d&#233;nutrition.</strong></p>
					  </div>
				  </div>
				  <div class="row">
				  	  <div class="col-xs-12 visible-xs text-center mb2">
					  	<p>
					  		<strong>La pr&#233;valence de la d&#233;nutrition prot&#233;ino-&#233;nerg&#233;tique est de 2 &#224; 5&nbsp;% chez les personnes &#226;g&#233;es vivant &#224; domicile.</strong>
					  	</p>
					  </div>
					  <div class="col-xs-12 col-sm-4">
					  	<form id="quizCol1" class="formQuiz" action="#">
					  	<div class="customCheckBox resultFalse">
					  		<label class="control control--radio gtmQuestionIncorrectAnswer" data-gtm="quizPopin-1-vrai">
								<input data-result="false" type="radio" name="radio" /> <span class="labelAnswer">Vrai</span>
								<div class="control__indicator"></div>
							</label>
					  	</div>
					  	<div class="customCheckBox resultCorrect">
					  		<label class="control control--radio gtmQuestionCorrectAnswer" data-gtm="quizPopin-1-faux">
								<input data-result="correct" type="radio" name="radio" />	<span class="labelAnswer">Faux</span>
								<div class="control__indicator"></div>
							</label>
					  	</div>
					  	</form>
					  	<p class="quizAnswer">
					  		<span></span><br/>
					  		En effet, la prévalence de la dénutrition protéino-énergétique est de 4 à 10 % chez les personnes âgées vivant à domicile, de 15 à 38 % chez celles vivant en institution et de 30 à 70 % chez les malades âgés hospitalisés<sup>1</sup>.
					  	</p>
					  </div>
					  <div class="col-xs-12 visible-xs text-center mt2 mb2">
					  	<p><strong>L&#8217;isolement social est une situation &#224; risque de d&#233;nutrition.</strong></p>
					  </div>
					  
					  <div class="col-xs-12 col-sm-4">
					  	<form id="quizCol2" action="#">
					  	<div class="customCheckBox resultCorrect">
					  		<label class="control control--radio gtmQuestionCorrectAnswer" data-gtm="quizPopin-2-vrai">
								<input data-result="correct" type="radio" name="radio" /> <span class="labelAnswer">Vrai</span>
								<div class="control__indicator"></div>
							</label>
					  	</div>
					  	<div class="customCheckBox resultFalse">
					  		<label class="control control--radio gtmQuestionIncorrectAnswer" data-gtm="quizPopin-2-faux">
								<input data-result="false" type="radio" name="radio" />	<span class="labelAnswer">Faux</span>
								<div class="control__indicator"></div>
							</label>
					  	</div>
					  	</form>
					  	<p class="quizAnswer">
					  		<span></span><br/>
					  		Chez la personne âgée, la dénutrition peut effectivement entraîner ou aggraver un état de fragilité ou de dépendance, et elle favorise la survenue de morbidités<sup>1</sup>.
					  	</p>
					  </div>
					  <div class="col-xs-12 visible-xs text-center mt2 mb2">
					  	<p>
					  		<strong>La d&#233;nutrition peut entra&#238;ner ou aggraver la fragilit&#233; physique.</strong>
					  	</p>
					  </div>
					  <div class="col-xs-12 col-sm-4">
					  	<form id="quizCol3" action="#">
					  	<div class="customCheckBox resultCorrect">
					  		<label class="control control--radio gtmQuestionCorrectAnswer" data-gtm="quizPopin-3-vrai">
								<input data-result="correct" type="radio" name="radio" /> <span class="labelAnswer">Vrai</span>
								<div class="control__indicator"></div>
							</label>
					  	</div>
					  	<div class="customCheckBox resultFalse">
					  		<label class="control control--radio gtmQuestionIncorrectAnswer" data-gtm="quizPopin-3-faux">
								<input data-result="false" type="radio" name="radio" />	<span class="labelAnswer">Faux</span>
								<div class="control__indicator"></div>
							</label>
					  	</div>
					  	</form>
					  	<p class="quizAnswer">
					  		<span></span><br/>
					  		Certaines situations peuvent favoriser la dénutrition ou y être associées et l'isolement social en fait partie<sup>1</sup>. Retrouvez les différentes situations qui doivent vous alerter dans l'onglet « <a class="secondary-color closeButton gtmClickNavigate" href="#situations-a-risque">Situations à risque</a> ».
					  	</p>
					  </div>
				  </div>

				  <div class="absolutePosition bottom left abbreviation secondary-color visible-sm pl0">
						<p class="small">
						Source : HAS (cf R&#233;f&#233;rences)<br>
						IMC : indice de masse corporelle</p>
			    	</div>
				  
			</div>
			<div class="absolutePosition bottom left abbreviation secondary-color hidden-sm">
				<p class="small">
				Source : HAS (cf R&#233;f&#233;rences)<br>
				IMC : indice de masse corporelle</p>
	    	</div>	
			<div class="absolutePosition bottom center secondary-color text-center">
	    		<center><button class="closeButton mt5">Fermer</button></center>
	    	</div>
	    	<div class="absolutePosition bottom right reference secondary-color">
	    		<a class="secondary-color text-uppercase" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	    	</div>
		</div>

		<div class="lightbox" id="popinProtocole">
			<div class="closePopin">
				<span>Fermer</span>
			</div>
			<div class="container pl5 pr5">
				<h2>Le seul CNO HPHE concentr&#233;</h2>
				<h3 class="text-uppercase">
		    	  	dont l&#8217;efficacit&#233; a &#233;t&#233; prouv&#233;e<sup>2</sup>
	    	  	</h3>
				<h3 class="mb4 text-center">
	    	  		Protocole de l&#8217;&#233;tude &#171;&nbsp;Efficacit&#233; et tol&#233;rance d'un nouveau compl&#233;ment nutritionnel hyperconcentr&#233; en prot&#233;ines et en &#233;nergie chez les sujets &#226;g&#233;s&nbsp;&#187;<sup>2</sup>
    	  		</h3>
    	  		<p>
    	  			&#201;tude prospective ouverte, bi-centrique (Nice et Menton), r&#233;alis&#233;e sur 30 jours aupr&#232;s de 21 patients (19 femmes et 2 hommes) &#226;g&#233;s de &#8805; 70&nbsp;ans. Les patients &#233;taient d&#233;nutris (perte de poids >&nbsp;5&nbsp;% en 1&nbsp;mois ou 10&nbsp;% en 6 mois et/ou IMC&nbsp;<&nbsp;21&nbsp;kg/m<sup>2</sup>) et/ou avec une diminution des ingesta (&#8804; 70&nbsp;% des besoins &#233;nerg&#233;tiques totaux estim&#233;s).
    	  		</p>
    	  		<p>
    	  			&#192; la sortie de l&#8217;h&#244;pital, les patients recevaient Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml &#224; prendre quotidiennement. &#192; J0 et &#224; J30, le poids, les ingesta spontan&#233;s, l&#8217;app&#233;tit (note de 0 &#224; 10), la force musculaire (hand grip) et la qualit&#233; de vie (questionnaire EQ-5D-5L) &#233;taient &#233;valu&#233;s.
    	  		</p>
    	  		<p class="mb0">Crit&#232;res d&#8217;exclusion :</p>
    	  		<ul class="customList">
    	  			<li><span>Les troubles primitifs du comportement alimentaire</span></li>
    	  			<li><span>Les cancers &#233;volutifs</span></li>
    	  			<li><span>Les troubles cognitifs s&#233;v&#232;res</span></li>
    	  		</ul>
    	  		<div class="absolutePosition bottom left abbreviation secondary-color visible-sm pl0">
					<p class="small">
					CNO : compl&#233;ment nutritionnel oral<br>
					HPHE : hyperprotidique hyper&#233;nerg&#233;tique<br>
					IMC : indice de masse corporelle
					</p>
		    	</div>
			</div>
			<div class="absolutePosition bottom left abbreviation secondary-color hidden-sm">
				<p class="small">
				CNO : compl&#233;ment nutritionnel oral<br>
				HPHE : hyperprotidique hyper&#233;nerg&#233;tique<br>
				IMC : indice de masse corporelle
				</p>
	    	</div>
			<div class="absolutePosition bottom center secondary-color text-center">
	    		<center><button class="closeButton mt5">Fermer</button></center>
	    	</div>
	    	<div class="absolutePosition bottom right reference secondary-color">
	    		<a class="secondary-color text-uppercase" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	    	</div>
		</div>

		<div class="lightbox" id="popinObservance">
			<div class="closePopin">
				<span>Fermer</span>
			</div>
			<div class="container pl5 pr5">
				<h2 class="mt4">Un CNO adapt&#233;</h2>
				<h3 class="text-uppercase">
		    	  	&#192; VOS PATIENTS D&#201;NUTRIS<br>POUR FAVORISER L'OBSERVANCE
	    	  	</h3>
				<div class="row">
	    	  		<div class="col-xs-0 col-md-3"></div>
	    	  		<div class="col-xs-12 col-md-6">
	    	  			<p class="text-center big">
	    	  				<strong class="secondary-color">
	    	  					Dans une &#233;tude clinique, Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml &#233;tait associ&#233; &#224; une grande satisfaction<sup>*</sup> des patients<sup>2</sup>
	    	  				</strong>
	    	  			</p>
	    	  		</div>
	    	  		<div class="col-xs-0 col-md-3"></div>
    	  		</div>
				<div class="row mt4 mb4">
	    	  		<div class="col-xs-0 col-sm-1"></div>
	    	  		<div class="col-xs-12 col-sm-push-5 col-sm-5">
	    	  			<img src="<?php print base_path() . path_to_theme(); ?>/dist/images/<?php echo $language->language; ?>/graph_popinObservance.png" alt="">
	    	  		</div>
	    	  		<div class="col-xs-12 col-sm-pull-5 col-sm-5">
	    	  			<img width="90%" src="<?php print base_path() . path_to_theme(); ?>/dist/images/<?php echo $language->language; ?>/img_popinObservance.png" alt="">
	    	  		</div>
	    	  		
	    	  		<div class="col-xs-0 col-sm-1"></div>
    	  		</div>
    	  		<div class="absolutePosition bottom left abbreviation secondary-color visible-sm pl0">
					<p class="small">
						<sup>*</sup> Crit&#232;res d&#8217;&#233;valuation : prise en main, ouverture, fermeture, apparence, praticit&#233;, go&#251;t, texture, volume, &#233;valuation globale
					</p>
		    	</div>
			</div>
			<div class="absolutePosition bottom left abbreviation secondary-color hidden-sm">
				<p class="small">
					<sup>*</sup> Crit&#232;res d&#8217;&#233;valuation : prise en main, ouverture, fermeture, apparence, praticit&#233;, go&#251;t, texture, volume, &#233;valuation globale
				</p>
	    	</div>
			<div class="absolutePosition bottom center secondary-color text-center">
	    		<center><button class="closeButton mt5">Fermer</button></center>
	    	</div>
	    	<div class="absolutePosition bottom right reference secondary-color">
	    		<a class="secondary-color text-uppercase" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	    	</div>
		</div>

		<div class="lightbox" id="popinReferences">
			<div class="closePopin">
				<span>Fermer</span>
			</div>
			<div class="container pl5 pr5">
				<h3 class="text-uppercase mb2">
		    	  	R&#233;f&#233;rences
	    	  	</h3>
    	  		<p>
    	  			1. HAS. Strat&#233;gie de prise en charge en cas de d&#233;nutrition prot&#233;ino-&#233;nerg&#233;tique chez la personne &#226;g&#233;e.
					Recommandations professionnelles. Avril 2007.<br>
					2. H&#233;buterne X, et al. Efficacit&#233; et tol&#233;rance d'un nouveau compl&#233;ment nutritionnel hyperconcentr&#233; en prot&#233;ines et
					en &#233;nergie chez les sujets &#226;g&#233;s. Poster pr&#233;sent&#233; au congr&#232;s des JFN. 2016; P097.<br>
					3. Arr&#234;t&#233; du 2 d&#233;cembre 2009, JO du 8 d&#233;cembre 2009.<br>
					4. Arr&#234;t&#233; du 25 juillet 2014, JO du 19 ao&#251;t 2014.
				</p>
				<p class="mt3">
				Les produits Fortimel<sup>&#174;</sup> sont des aliments di&#233;t&#233;tiques destin&#233;s &#224; des fins m&#233;dicales sp&#233;ciales. &#192; utiliser sous contr&#244;le m&#233;dical.<br> Les produits Fortimel<sup>&#174;</sup> sont pris en charge au titre de la LPP en cas de d&#233;nutrition.
    	  		</p>
    	  		<p>
    	  			NUTRICIA Nutrition Clinique, 150 boulevard Victor-Hugo, 93406 Saint-Ouen Cedex
    	  		</p>
    	  		<p class="job">17-05-17 6#7</p>
			</div>
			<div data-menuanchor="produit" class="absolutePosition bottom center secondary-color text-center">
	    		<center><button class="closeButton mt5">Fermer</button></center>
	    	</div>
		</div>
