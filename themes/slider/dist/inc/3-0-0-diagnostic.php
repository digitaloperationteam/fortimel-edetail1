 <section id="diagnostic_04" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S004" data-screen-label="Diagnostic" data-screen-name="Diagnostic" data-screen-section="">
    <div class="slide fp-auto-height">
    	
    	<div class="container">
	    	<h2>
    	  		Vos patients &#226;g&#233;s
    	  	</h2>
    	  	<h3 class="text-uppercase">
	    	  	souffrent-ils de d&#233;nutrition&nbsp;?
    	  	</h3>
		  <div class="row">
		  	  <div class="col-xs-12 col-md-6 visible-xs">
			  	<img class="rightImage responsive mt0 mb2 noMax" width="415" src="images/img_patients-ages.png" alt="">
			  </div>
			  <div class="col-xs-12 col-sm-6 col-md-6">
			  	<p class="mb2">La HAS recommande de r&#233;aliser au minimum un d&#233;pistage annuel de la d&#233;nutrition chez toutes les personnes &#226;g&#233;es<sup>1</sup>. N&#233;anmoins, une prise en charge nutritionnelle sera d&#8217;autant plus efficace qu&#8217;elle sera pr&#233;coce<sup>1</sup>. Il est particuli&#232;rement recommand&#233; aux m&#233;decins traitants de peser les patients &#226;g&#233;s &#224; chaque consultation m&#233;dicale<sup>1</sup>.</p> 
				
				<p class="mb0"><strong class="secondary-color font-black">Le diagnostic de d&#233;nutrition des patients &gt; 70 ans repose sur la pr&#233;sence d&#8217;un ou de plusieurs des crit&#232;res suivants<sup>1</sup>&nbsp;:</strong></p>
				
				<ul class="customList">
					<li><span>Perte de poids &#8805; 5&nbsp;% en 1 mois ou &#8805; 10&nbsp;% en 6 mois</span></li>
					<li><span>IMC &lt; 21&nbsp;kg/m<sup>2</sup> (un IMC &#8805; 21&nbsp;kg/m<sup>2</sup> n&#8217;exclut pas le diagnostic de d&#233;nutrition)</span></li>
					<li><span>Albumin&#233;mie &lt; 35 g/l<sup>*</sup></span></li>
					<li><span>Mini Nutritional Assessment (MNA) global &#8804; 17</span></li>
				</ul>
				

				<p class="mb0"><strong class="secondary-color font-black">D&#233;nutrition s&#233;v&#232;re<sup>1</sup>&nbsp;:</strong></p>
				<ul class="customList">
					<li><span>Perte de poids &#8805; 10&nbsp;% en 1 mois ou &#8805; 15&nbsp;% en 6 mois</span></li>
					<li><span>IMC &lt; 18&nbsp;kg/m<sup>2</sup></span></li>
					<li><span>Albumin&#233;mie &lt; 30&nbsp;g/l</span></li>
				</ul>

				<p class="mb4">En cas de d&#233;nutrition la HAS pr&#233;conise, dans un premier temps, l'enrichissement de l'alimentation. En cas d'&#233;chec, des compl&#233;ments nutritionnels oraux peuvent &#234;tre prescrits<sup>1</sup>.</p>

			  </div>
			  <div class="col-xs-12 col-sm-6 col-md-6">
			  	<img class="rightImage mb2 noMax hidden-xs" width="715" src="images/img_patients-ages.png" alt="">
			  	<p class="text-center"><button data-featherlight="#popinQuiz" class="primary-color gtmClickPopin" data-gtm="testezvous-popin">Testez-vous</button></p>
			  </div>
			</div>
		</div>
	</div>
	<div class="absolutePosition bottom left abbreviation secondary-color">
	<p class="small mb0">
	IMC : indice de masse corporelle<br>
	* Interpr&#233;ter le dosage de l&#8217;albumin&#233;mie en tenant compte de l&#8217;&#233;tat  inflammatoire du malade, &#233;valu&#233; avec le dosage de la prot&#233;ine C-r&#233;active</p>
	</div>	
	<div data-menuanchor="efficacite-clinique" class="absolutePosition bottom center nextPage secondary-color text-center">
		<a class="secondary-color text-uppercase gtmClickNavigate" data-gtm="nextPage-nav" href="#efficacite-clinique">Pourquoi prescrire <br>Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml&nbsp;?</a>
	</div>
	<div class="absolutePosition bottom right reference secondary-color">
		<a class="secondary-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>
</section>