<section id="produit_06" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S005" data-screen-label="Produit" data-screen-name="Produit" data-screen-section="">
	<div class="slide fp-auto-height">
	  	<div class="container">
			<h2 class="pl3 pr3">
			Faites confiance &#224; un CNO
			</h2>
			<h3 class="text-uppercase white-color">
			QUI A FAIT SES PREUVES <br>ET QUE LES PATIENTS APPR&#201;CIENT
			</h3>
		  <div class="row">
	    	  	<div class="col-xs-12 col-md-4 white-color">
	    	  		<p class="logo mb0 text-right"><img class="mb4" width="240" src="images/logo_nutricia.png" alt=""></p>
	    	  		<p class="white-color text-right hidden-xs hidden-sm">
	    	  			<strong class="text-uppercase">Disponible en une large vari&#233;t&#233; d&#8217;ar&#244;mes savoureux </strong><br>
	    	  			pour satisfaire les patients et favoriser l&#8217;observance
	    	  		</p>
	    	  	</div>
	    	  	<div class="col-xs-4 col-md-4 white-color text-center product">
	    	  		<img width="270" src="images/produit_clinique_bottle.png" alt="">
	    	  	</div>
	    	  	<div class="col-xs-8 col-md-4 white-color">
	    	  		<p class="white-color visible-xs visible-sm">
	    	  			<strong class="text-uppercase">Disponible en une large vari&#233;t&#233; d&#8217;ar&#244;mes savoureux </strong><br>
	    	  			pour satisfaire les patients et favoriser l&#8217;observance
	    	  		</p>
	    	  		<p class="white-color text-left mb1">
	    	  			<strong  class="text-uppercase mb1">Une concentration &#233;lev&#233;e en prot&#233;ines et &#201;NERGIE</strong>
	    	  		</p>
	    	  		<p class="white-color text-left mb2">
	    	  			29 g de prot&#233;ines et 480 kcal en seulement 200&nbsp;ml&nbsp;- particuli&#232;rement adapt&#233;e aux besoins des patients d&#233;nutris
	    	  		</p>
	    	  		<p class="white-color text-left">
		    	  		<strong  class="text-uppercase">
		    	  			LE SEUL CNO HPHE concentr&#233; &#224; l&#8217;efficacit&#233; cliniquement prouv&#233;e
		    	  			<sup>2</sup>
		    	  		</strong>
	    	  		</p>
	    	  	</div>
	  		</div>
	  	</div>
  	</div>
  	<div class="absolutePosition bottom left abbreviation secondary-color">
	<p class="small white-color mb0">
	CNO : compl&#233;ment nutritionnel oral<br>
	HPHE : hyperprotidique hyper&#233;nerg&#233;tique</p>
	</div>	
  	<div data-menuanchor="gamme" class="absolutePosition bottom center nextPage white-color text-center">
		<a class="white-color gtmClickNavigate" data-gtm="nextPage-nav" href="#gamme">D&#201;COUVREZ <br>LA GAMME FORTIMEL<sup>&#174;</sup></a>
	</div>
	<div class="absolutePosition bottom right reference white-color">
		<a class="white-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>		
</section>