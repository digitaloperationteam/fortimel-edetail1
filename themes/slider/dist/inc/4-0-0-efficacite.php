<section id="efficacite_05" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S004" data-screen-label="Efficacit&#233;" data-screen-name="Efficacit&#233;" data-screen-section="">
	<div class="slide fp-auto-height">
	  	<div class="container">
    	  	<h2>
    	  		Le seul CNO HPHE concentr&#233;
    	  	</h2>
    	  	<h3 class="text-uppercase mb2">
	    	  	dont l&#8217;efficacit&#233; a &#233;t&#233; prouv&#233;e<sup>2</sup>
    	  	</h3>
		  <div class="row">
			  <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">	
				<p class="text-center">
					 <img src="images/graph_efficacite.png" alt=""><br>
					 <strong>&#201;volution du poids et de la force musculaire<br> apr&#232;s 30 jours<sup>2</sup></strong>  (Donn&#233;es en m&#233;diane [extr&#234;mes])
				</p>
			  </div>
			  <div class="col-xs-12 col-sm-6 col-md-6">
			  	<div class="pt5 hidden-xs hidden-sm">&nbsp;</div>
			  	<p class="mb0">Dans une &#233;tude clinique r&#233;alis&#233;e chez des patients &#226;g&#233;s d&#233;nutris ou &#224; risque de d&#233;nutrition, la prise quotidienne de Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml pendant 30 jours a permis<sup>2</sup>&nbsp;:</p>
				<ul class="mb2 customList">
					<li><span>Une augmentation m&#233;diane du poids de 2,6&nbsp;%</span></li>
					<li><span>Une am&#233;lioration m&#233;diane de la force musculaire de 5,2&nbsp;%</span></li>
				</ul>

				<p class="mb2">Ces deux facteurs sont importants pour le maintien de l&#8217;autonomie.</p>
				<p class="mb2"><button data-featherlight="#popinProtocole" class="primary-color mb0 hidden-xs gtmClickPopin" data-gtm="protocoleEtude-popin">Protocole de l&#8217;&#233;tude</button></p>
			  </div>
			  <div class="col-xs-12 col-md-6 visible-xs">	
				<p class="text-center">
					 <img src="images/graph_efficacite.png" alt="">
					 <strong>&#201;volution du poids et de la force musculaire<br> apr&#232;s 30 jours<sup>2</sup></strong>  (Donn&#233;es en m&#233;diane [extr&#234;mes])
				</p>
				<p class="mb2 mt2 text-center"><button data-featherlight="#popinProtocole" class="primary-color mb0 gtmClickPopin" data-gtm="protocoleEtude-popin">Protocole de l&#8217;&#233;tude</button></p>
			  </div>
		  </div>
		</div>
	</div>
	<div class="absolutePosition bottom left abbreviation secondary-color">
	<p class="small mb0">
	CNO&nbsp;: compl&#233;ment nutritionnel oral<br>
	HPHE&nbsp;: hyperprotidique hyper&#233;nerg&#233;tique</p>
	</div>	
	<div data-menuanchor="produit-clinique" class="absolutePosition bottom center nextPage secondary-color text-center">
		<a class="secondary-color text-uppercase gtmClickNavigate" data-gtm="nextPage-nav" href="#produit">UN CNO <br>QUI A FAIT SES PREUVES</a>
	</div>
	<div class="absolutePosition bottom right reference secondary-color">
		<a class="secondary-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>
</section>