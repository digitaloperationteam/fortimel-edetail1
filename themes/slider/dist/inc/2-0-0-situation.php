<section id="situation_03" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S003" data-screen-label="Situations" data-screen-name="Situations" data-screen-section="">
	<div class="container visible-xs visible-sm">
		<h2>
    	  		Les situations &#224; risque
	  	</h2>
	  	<h3 class="text-uppercase visible-xs visible-sm">
    	  	Qui peuvent entra&#238;ner la d&#233;nutrition<sup>1</sup>
	  	</h3>
	</div>
	<div class="slide noSlideForResponsive" data-anchor="1">
		<div class="container">
    		<h2 class="hidden-xs  hidden-sm">
    	  		Les situations &#224; risque
    	  	</h2>
    	  	<h3 class="text-uppercase hidden-xs hidden-sm">
	    	  	Qui peuvent entra&#238;ner la d&#233;nutrition<sup>1</sup>
    	  	</h3>
    	  	<div class="row">
	    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
	    	  	<div class="autoWidth">
		    	  	<center><img width="118" src="images/icon-risque-psycho.png" alt=""></center>
		    	  	<p class="mb0">
		    	  		<strong class="font-black secondary-color">
		    	  			Psycho-socio-environnementales
		    	  		</strong>
		    	  	</p>
		    	  	<ul class="customList">
		    	  		<li><span>Isolement social</span></li>
		    	  		<li><span>Deuil</span></li>
		    	  		<li><span>Difficult&#233;s financi&#232;res</span></li>
		    	  		<li><span>Maltraitance</span></li>
		    	  		<li><span>Hospitalisation</span></li>
		    	  		<li><span>Changement des habitudes de vie&nbsp; (ex.&nbsp;: entr&#233;e en institution)</span></li>
		    	  	</ul>
	    	  	</div>
	    	  </div>
	    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
	    	  	<div class="autoWidth">
		    	  	<center><img width="118" src="images/icon-risque-dentaire.png" alt=""></center>
		    	  	<p class="mb0">
		    	  		<strong class="font-black secondary-color">
		    	  			Troubles bucco-dentaires
		    	  		</strong>
		    	  	</p>
		    	  	<ul class="customList">
		    	  		<li><span>Troubles de la mastication</span></li>
		    	  		<li><span>Mauvais &#233;tat dentaire</span></li>
		    	  		<li><span>Appareillage mal adapt&#233;</span></li>
		    	  		<li><span>S&#233;cheresse de la bouche</span></li>
		    	  		<li><span>Candidose oro-pharyng&#233;e</span></li>
		    	  		<li><span>Dysgueusie</span></li>
		    	  	</ul>
	    	  	</div>
	    	  </div>
	    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
	    	  	<div class="autoWidth">
		    	  	<center><img width="118" src="images/icon-risque-deglutition.png" alt=""></center>
		    	  	<p class="mb0">
		    	  		<strong class="font-black secondary-color">
		    	  			Troubles de la d&#233;glutition
		    	  		</strong>
		    	  	</p>
		    	  	<ul class="customList">
		    	  		<li><span>Pathologie ORL</span></li>
		    	  		<li><span>Pathologie neurod&#233;g&#233;n&#233;rative <br>ou vasculaire</span></li>
		    	  	</ul>
	    	  	</div>
	    	  </div>
	    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
	    	  	<div class="autoWidth">
		    	  	<center><img width="118" src="images/icon-risque-psychiatrique.png" alt=""></center>
		    	  	<p class="mb0">
		    	  		<strong class="font-black secondary-color">
		    	  			Troubles psychiatriques
		    	  		</strong>
		    	  	</p>
		    	  	<ul class="customList">
		    	  		<li><span>Syndromes d&#233;pressifs</span></li>
		    	  		<li><span>Troubles du comportement</span></li>
		    	  	</ul>
	    	  	</div>
	    	  </div>
	    	</div>
    	</div>
	</div>
	<div class="slide noSlideForResponsive" data-anchor="2">
		<div class="container">
			<h2 class="hidden-xs hidden-sm">
    	  		Les situations &#224; risque
    	  	</h2>
    	  	<h3 class="text-uppercase hidden-xs hidden-sm">
	    	  	Qui peuvent entra&#238;ner la d&#233;nutrition<sup>1</sup>
    	  	</h3>
    	  	<div class="row">
		    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
		    	  	<div class="autoWidth">
			    	  	<center><img class="mb2" width="118" src="images/icon-risque-dementiel.png" alt=""></center>
			    	  	<p class="mb0">
			    	  		<strong class="font-black secondary-color">
			    	  			Syndromes d&#233;mentiels
			    	  		</strong>
			    	  	</p>
			    	  	<ul class="customList">
			    	  		<li><span>Maladie d&#8217;Alzheimer</span></li>
			    	  		<li><span>Autres d&#233;mences</span></li>
			    	  	</ul>
		    	  	</div>
		    	  </div>
		    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
		    	  	<div class="autoWidth">
			    	  	<center><img class="mb2" width="118" src="images/icon-risque-neuro.png" alt=""></center>
			    	  	<p class="mb0">
			    	  		<strong class="font-black secondary-color">
			    	  			Autres troubles neurologiques
			    	  		</strong>
			    	  	</p>
			    	  	<ul class="customList">
			    	  		<li><span>Syndrome confusionnel</span></li>
			    	  		<li><span>Troubles de la vigilance</span></li>
			    	  		<li><span>Syndrome parkinsonien</span></li>
			    	  	</ul>
		    	  	</div>
		    	  </div>
		    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
		    	  	<div class="autoWidth">
			    	  	<center><img class="mt2" width="118" src="images/icon-risque-traitement.png" alt=""></center>
			    	  	<p class="mb0">
			    	  		<strong class="font-black secondary-color">
			    	  			Traitements m&#233;dicamenteux au long cours
			    	  		</strong>
			    	  	</p>
			    	  	<ul class="customList">
			    	  		<li><span>Polym&#233;dication</span></li>
			    	  		<li><span>M&#233;dicaments entra&#238;nant une s&#233;cheresse de la bouche, une dysgueusie, des troubles digestifs, une anorexie, une somnolence&#8230;</span></li>
			    	  		<li><span>Cortico&#239;des au long cours</span></li>
			    	  	</ul>
		    	  	</div>
		    	  </div>
		    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
		    	  	<div class="autoWidth">
			    	  	<center><img class="mb2" width="118" src="images/icon-risque-affection.png" alt=""></center>
			    	  	<p class="mb0">
			    	  		<strong class="font-black secondary-color">
			    	  			Toute affection aigu&#235; ou d&#233;compensation d&#8217;une pathologie chronique
			    	  		</strong>
			    	  	</p>
			    	  	<ul class="customList">
			    	  		<li><span>Douleur</span></li>
			    	  		<li><span>Pathologie infectieuse</span></li>
			    	  		<li><span>Fracture entra&#238;nant une impotence fonctionnelle</span></li>
			    	  		<li><span>Intervention chirurgicale</span></li>
			    	  		<li><span>Constipation s&#233;v&#232;re</span></li>
			    	  		<li><span>Escarres</span></li>
			    	  	</ul>
		    	  	</div>
		    	  </div>
	    	</div>
    	</div>
	</div>
	<div class="slide noSlideForResponsive" data-anchor="3">
		<div class="container">
			<h2 class="hidden-xs hidden-sm">
    	  		Les situations &#224; risque
    	  	</h2>
    	  	<h3 class="text-uppercase hidden-xs hidden-sm">
	    	  	Qui peuvent entra&#238;ner la d&#233;nutrition<sup>1</sup>
    	  	</h3>
	    	  	<div class="row">
		    	  <div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-3 slideForResponsive">
		    	  	<div class="autoWidth">
			    	  	<center><img width="118" src="images/icon-risque-dependance.png" alt=""></center>
			    	  	<p class="mb0">
			    	  		<strong class="font-black secondary-color">
			    	  			D&#233;pendance pour les actes de la vie quotidienne
			    	  		</strong>
			    	  	</p>
			    	  	<ul class="customList">
			    	  		<li><span>D&#233;pendance pour l&#8217;alimentation</span></li>
			    	  		<li><span>D&#233;pendance pour la mobilit&#233;</span></li>
			    	  	</ul>
		    	  	</div>
		    	  </div>
		    	  <div class="col-xs-12 col-sm-3 col-md-3 slideForResponsive">
		    	  	<div class="autoWidth">
			    	  	<center><img width="118" src="images/icon-risque-regimes.png" alt=""></center>
			    	  	<p class="mb0">
			    	  		<strong class="font-black secondary-color">
			    	  			R&#233;gimes restrictifs
			    	  		</strong>
			    	  	</p>
			    	  	<ul class="customList">
			    	  		<li><span>Sans sel</span></li>
			    	  		<li><span>Amaigrissant</span></li>
			    	  		<li><span>Diab&#233;tique</span></li>
			    	  		<li><span>Hypocholest&#233;rol&#233;miant</span></li>
			    	  		<li><span>Sans r&#233;sidu au long cours</span></li>
			    	  	</ul>
		    	  	</div>
		    	  </div>
	    	</div>
	  	</div>
	</div>

	<div data-menuanchor="diagnostic" class="absolutePosition bottom center nextPage secondary-color text-center">
		<a class="secondary-color text-uppercase gtmClickNavigate" data-gtm="nextPage-nav" href="#diagnostic">COMMENT DIAGNOSTIQUER <br>LA D&#201;NUTRITION&nbsp;?</a>
	</div>
	<div class="absolutePosition bottom right reference secondary-color">
		<a class="secondary-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>	
</section>