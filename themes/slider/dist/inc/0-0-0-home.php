<section id="home_01" class="section" data-screen-id="2017_FOR_ED1_S001" data-screen-label="Home" data-screen-name="Home" data-screen-section="">
	<div class="slide fp-auto-height-responsive noSlideForResponsive">
		<div class="row">
		  <div class="col-xs-10 col-md-8 col-md-offset-2 col-xs-offset-1">
			<center><img class="hidden-xs" width="100%" src="images/img_homepage.png" alt="Rendez &#224; vos patients ce dont la d&#233;nutrition les prive"></center>
			<img class="mainImage visible-xs" src="images/mainImage_responsive.png" alt="">
		  </div>
		</div>
	</div>
	<div data-menuanchor="quiz" class="absolutePosition bottom center nextPage white-color text-center">
		<a class="white-color scrollBtn gtmClickNavigate" data-gtm="nextPage-nav" href="#quiz">D&#201;COUVREZ<br>COMMENT</a>
	</div>
	<div class="absolutePosition bottom right reference white-color">
		<a class="white-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>	    
</section>