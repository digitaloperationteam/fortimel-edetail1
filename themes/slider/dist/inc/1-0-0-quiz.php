<section id="quiz_02" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S002" data-screen-label="Quiz" data-screen-name="Quiz" data-screen-section="">
	<div class="slide fp-auto-height-responsive noSlideForResponsive">
    	<div class="title container">
	    	<h2>
    	  		Testez-vous
    	  	</h2>
    	  	<h3>
	    	  	Deux de ces patients pourraient b&#233;n&#233;ficier<br>
				de Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml<sup>*</sup>. Lesquels&nbsp;?	
    	  	</h3>
	  	</div>
    	<div id="allAnswers" class="container ">
		  <div class="row">
	    	  <div class="col-xs-12 col-sm-6 col-md-6 mb1 slideForResponsive">
	    	  	<div class="col-xs-12 col-sm-12 col-md-5">
	    	  		<img width="100%" src="images/madame_w.png" alt="">
	    	  	</div>
	    	  	<div class="col-xs-12 col-sm-12 col-md-7">
	    	  		<button data-target="0" class="mt3 gtmQuestionIncorrectAnswer" data-gtm="quiz-madame-w">Madame W.  <span class="primary-color">81 ans</span></button>
	    	  		<p>Elle vit seule mais son entourage est assez pr&#233;sent. Elle a perdu 2&nbsp;kg en 1 mois (3&nbsp;% de son poids). Depuis peu, elle a perdu tout app&#233;tit.</p>
	    	  	</div>
	    	  </div>
			  <div class="col-xs-12 col-sm-6 col-md-6 mb1 slideForResponsive">
			  	<div class="col-xs-12 col-sm-12 col-md-5">
			  		<img class="mt1" width="100%" src="images/monsieur_a.png" alt="">
			  	</div>
	    	  	<div class="col-xs-12 col-sm-12 col-md-7">
	    	  		<button data-target="1" class="mt3 gtmQuestionCorrectAnswer" data-gtm="quiz-monsieur-a">Monsieur A.  <span class="primary-color">75 ans</span></button>
	    	  		<p>Il vit seul depuis le d&#233;c&#232;s de sa femme il y a 3 mois. Il a r&#233;cemment chut&#233; sans cons&#233;quence importante et a perdu 5&nbsp;kg en 1 mois (6&nbsp;% de son poids).</p>
	    	  	</div>
			  </div>
		  </div>
		  <div class="row">
			  <div class="col-xs-12 col-sm-6 col-md-6 mb1 slideForResponsive">
			  	<div class="col-xs-12 col-sm-12 col-md-5">
			  		<img width="100%" src="images/monsieur_b.png" alt="">
			  	</div>
	    	  	<div class="col-xs-12 col-sm-12 col-md-7">
	    	  		<button data-target="2" class="mt3 gtmQuestionIncorrectAnswer" data-gtm="quiz-monsieur-b">Monsieur B.  <span class="primary-color">85 ans</span></button>
	    	  		<p>Depuis une op&#233;ration de la hanche le mois dernier, il a du mal &#224; se d&#233;placer. Des aides &#224; domicile lui pr&#233;parent ses repas quotidiennement, qu&#8217;il mange avec plaisir. Son poids reste stable.</p>
	    	  	</div>
			  </div>
			  <div class="col-xs-12 col-sm-6 col-md-6 mb1 slideForResponsive">
			  	<div class="col-xs-12 col-sm-12 col-md-5">
			  		<img width="100%" src="images/madame_c.png" alt="">
			  	</div>
	    	  	<div class="col-xs-12 col-sm-12 col-md-7">
	    	  		<button data-target="3" class="mt3 gtmQuestionCorrectAnswer" data-gtm="quiz-madame-c">Madame C.  <span class="primary-color">72 ans</span></button>
	    	  		<p>Elle dit se sentir en forme. Elle a remarqu&#233; que son app&#233;tit s&#8217;est d&#233;grad&#233; depuis plusieurs mois du fait d&#8217;un nouveau traitement m&#233;dicamenteux et son IMC est de 20&nbsp;kg/m<sup>2</sup>.</p>
	    	  	</div>
			  </div>
		  </div>
	  	</div>
	  	<div id="showAnwserContainer">
			<div id="answer0" class="showAnwser container pb0" style="display:none;">
				<div class="row">
					<div class="col-xs-12 col-md-2 visible-xs">
						<center><img width="100%" src="images/madame_w.png" alt=""></center>
					</div>
					<div class="col-xs-12 col-md-4 col-md-offset-1">
						<h4 class="mt2 text-right">Madame W. <span>81 ans</span></h4> 
							<p class="text-right">Elle vit seule mais son entourage est assez pr&#233;sent. Elle a perdu 2&nbsp;kg en 1 mois (3&nbsp;% de son poids). Depuis peu, elle a perdu tout app&#233;tit.</p>
					</div>
					<div class="col-xs-12 col-md-2 hidden-xs">
						<center><img width="100%" src="images/madame_w.png" alt=""></center>
					</div>
					<div class="col-xs-12 col-md-5">
						<p  class="mt2"><span class="correctOrFalse red">Faux ! </span>&nbsp;- <a class="retryQuiz" href="#">R&#233;essayer</a></p>
						<p>Le pourcentage de perte de poids de Madame W. est inf&#233;rieur au seuil d&#233;finissant une d&#233;nutrition. 
						</p>
						<p>Cependant, ses apports nutritionnels sont en train de diminuer, il est donc n&#233;cessaire de lui prodiguer des conseils di&#233;t&#233;tiques et d'enrichir son alimentation. Un suivi doit &#234;tre mis en place et en cas de d&#233;nutrition av&#233;r&#233;e une prescription de CNO pourra &#234;tre envisag&#233;e<sup>1</sup>.</p>
					</div>
				</div>
			</div>
			<div id="answer1" class="showAnwser container pb0" style="display:none;">
				<div class="row">
					<div class="col-xs-12 col-md-4 col-md-offset-1">
						<h4 class="mt2 text-right">Monsieur A.  <span class="primary-color">75 ans</span></h4> 
							<p class="text-right">Il vit seul depuis le d&#233;c&#232;s de sa femme il y a 3 mois. Il a r&#233;cemment chut&#233; sans cons&#233;quence importante et a perdu 5&nbsp;kg en 1 mois (6&nbsp;% de son poids).</p>
					</div>
					<div class="col-xs-12 col-md-2">
						<center><img width="100%" src="images/monsieur_a.png" alt=""></center>
					</div>
					<div class="col-xs-12 col-md-5">
						<p class="mt2"><span class="correctOrFalse green">Correct !</span>&nbsp;- <a class="retryQuiz" href="#">Retourner au quiz</a></p>
						<p>Monsieur A. a perdu 6&nbsp;% de son poids en 1 mois. Il est d&#233;nutri.</p>
						<p>En cas d'&#233;chec de l'enrichissement de l'alimentation, Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml pourrait aider ce patient &agrave; pr&#233;server son autonomie<sup>1</sup>.</p>
					</div>
				</div>
			</div>
			<div id="answer2" class="showAnwser container pb0" style="display:none;">
				<div class="row">
					<div class="col-xs-12 col-md-4 col-md-offset-1">
						<h4 class="mt2 text-right">Monsieur B.  <span class="primary-color">85 ans</span></h4> 
							<p class="text-right">Depuis une op&#233;ration de la hanche le mois dernier, il a du mal &#224; se d&#233;placer. Des aides &#224; domicile lui pr&#233;parent ses repas quotidiennement, qu&#8217;il mange avec plaisir. Son poids reste stable.</p>
					</div>
					<div class="col-xs-12 col-md-2">
						<center><img width="100%" src="images/monsieur_b.png" alt=""></center>
					</div>
					<div class="col-xs-12 col-md-5">
						<p class="mt2"><span class="correctOrFalse red">FAUX !</span>&nbsp;- <a class="retryQuiz" href="#">R&#233;essayer</a></p>
						<p>Monsieur B. ne pr&#233;sente aucun des crit&#232;res diagnostiques de la d&#233;nutrition. Une prise en charge nutritionnelle n'est pas justifi&#233;e dans ce cas<sup>1</sup>.</p>
					</div>
				</div>
			</div>
			<div id="answer3" class="showAnwser container pb0" style="display:none;">
				<div class="row">
					<div class="col-xs-12 col-md-4 col-md-offset-1">
						<h4 class="mt2 text-right">Madame C.  <span class="primary-color">72 ans</span></h4> 
							<p class="text-right">Elle dit se sentir en forme. Elle a remarqu&#233; que son app&#233;tit s&#8217;est d&#233;grad&#233; depuis plusieurs mois du fait d&#8217;un nouveau traitement m&#233;dicamenteux et son IMC est de 20&nbsp;kg/m<sup>2</sup>.</p>
					</div>
					<div class="col-xs-12 col-md-2">
						<center><img width="100%" src="images/madame_c.png" alt=""></center>
					</div>
					<div class="col-xs-12 col-md-5">
						<p class="mt2"><span class="correctOrFalse green">CORRECT !</span>&nbsp;- <a class="retryQuiz" href="#">Retourner au quiz</a></p>
						<p>Madame C. a un IMC de 20&nbsp;kg/m<sup>2</sup> et de plus son app&#233;tit est d&#233;grad&#233;. Elle est d&#233;nutrie.</p>
						<p>En cas d'&#233;chec de l'enrichissement de l'alimentation, Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml pourrait aider cette patiente &#224; pr&#233;server son autonomie<sup>1</sup>.</p>
					</div>
				</div>
			</div>
			<div id="footerAnswer" class="container mt2 mb4" style="display:none;">
				<h3 class="mb2">
		    	  	Comment diagnostiquer la d&#233;nutrition du sujet &#226;g&#233;&nbsp;?
	    	  	</h3>
	    	  	<p class="text-center mb2">
	    	  		Le diagnostic de d&#233;nutrition des patients &gt; 70 ans repose sur la pr&#233;sence d&#8217;un ou de plusieurs des crit&#232;res suivants<sup>1</sup>&nbsp;:
	    	  	</p>
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<ul class="customList">
							<li><span>Perte de poids&nbsp;&#8805;&nbsp;5&nbsp;% en 1 mois ou&nbsp;&#8805;&nbsp;10&nbsp;%&nbsp;en 6&nbsp;mois</span></li>
						</ul>
					</div>
					<div class="col-xs-12 col-md-3">
						<ul class="customList">
							<li><span>IMC &lt;&nbsp;21&nbsp;kg/m<sup>2</sup>  (un&nbsp;IMC&nbsp;&#8805;&nbsp;21&nbsp;kg/m<sup>2</sup>&#160;n&#8217;exclut pas le diagnostic de d&#233;nutrition)</span></li>
						</ul> 
					</div>
					<div class="col-xs-12 col-md-3">
						<ul class="customList">
							<li><span>Albumin&#233;mie &lt;&nbsp;35&nbsp;g/l**</span></li>
						</ul>
					</div>
					<div class="col-xs-12 col-md-3">
						<ul class="customList">
							<li><span>Mini Nutritional Assessment (MNA)&nbsp;global&nbsp;&#8804;&nbsp;17</span></li>
						</ul> 
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="absolutePosition bottom left abbreviation secondary-color">
	<p class="small mb0">
	CNO : compl&#233;ment nutritionnel oral<br>
	IMC : indice de masse corporelle<br>
	* Apr&#232;s &#233;chec de l'enrichissement de l'alimentation<br>
	** Interpr&#233;ter le dosage de l&#8217;albumin&#233;mie en tenant compte  de l&#8217;&#233;tat inflammatoire du malade, &#233;valu&#233; avec le dosage de la prot&#233;ine C-r&#233;active</p>
	</div>	
	<div data-menuanchor="situation-a-risque" class="absolutePosition bottom center nextPage secondary-color text-center">
		<a class="secondary-color text-uppercase scrollBtn gtmClickNavigate" data-gtm="nextPage-nav" href="#situations-a-risque">D&#233;couvrez les situations <br>qui doivent vous alerter</a>
	</div>
	<div class="absolutePosition bottom right reference secondary-color">
		<a class="secondary-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>	
</section>