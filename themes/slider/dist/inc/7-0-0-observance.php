<section id="observance_08" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S007" data-screen-label="Observance" data-screen-name="Observance" data-screen-section="">
	<div class="slide fp-auto-height">
	  	<div class="container">
	  	<h2>
	  		Un CNO adapt&#233;
	  	</h2>
	  	<h3 class="text-uppercase mb0">
    	  	&#192; VOS PATIENTS D&#201;NUTRIS<br>POUR FAVORISER L'OBSERVANCE
	  	</h3>
		  <div class="row">
	    	  	<div class="col-xs-12 col-md-5">
	    	  		<p class="secondary-color mt4">
	    	  			<strong>Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml est particuli&#232;rement adapt&#233; &#224; vos patients d&#233;nutris pour favoriser l'observance du traitement nutritionnel&nbsp;:</strong>
	    	  		</p>
	    	  		<ul class="customList">
		    	  		<li><span>Dans une &#233;tude clinique, Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml &#233;tait associ&#233; &#224; une bonne observance&nbsp;: la moiti&#233; des patients ont consomm&#233; au moins 97,5&nbsp;% de la quantit&#233; prescrite<sup>2</sup>.</span></li>
		    	  		<li><span>Disponible en 7 ar&#244;mes savoureux pour satisfaire tous les patients et favoriser l&#8217;observance.</span></li>
		    	  		<li><span>Il peut &#234;tre int&#233;gr&#233; &#224; des recettes pour enrichir facilement les repas.</span></li>
		    	  	</ul>
	    	  	</div>
	    	  	<div class="col-xs-12 col-sm-6 col-md-2 white-color text-center pl1 pr1">
	    	  		<img class="product" width="100%" src="images/produit_observance.png" alt="">
	    	  	</div>
	    	  	<div class="col-xs-12 col-sm-6 col-md-5 white-color">
	    	  		<p class="text-center secondary-color mt2">
	    	  			<strong>Ar&#244;mes disponibles</strong>
	    	  		</p>
	    	  		<div class="row mb1">
	    	  			<div class="col-xs-4 col-md-4">
	    	  				<p class="text-center">
	    	  					<img width="100%" src="images/icon_caramel.png" alt="">
	    	  					<strong class="primary-color">Caramel</strong>
	    	  				</p>
	    	  			</div>
	    	  			<div class="col-xs-4 col-md-4">
	    	  				<p class="text-center">
	    	  					<img width="100%" src="images/icon_vanille.png" alt="">
	    	  					<strong class="primary-color">Vanille</strong>
	    	  				</p>
	    	  			</div>
	    	  			<div class="col-xs-4 col-md-4">
							<p class="text-center">
								<img width="100%" src="images/icon_banane.png" alt="">
								<strong class="primary-color">Banane</strong>
							</p>
	    	  			</div>
	    	  		</div>
	    	  		<div class="row">
	    	  			<div class="col-xs-4 col-md-4">
	    	  				<p class="text-center">
		    	  				<img width="100%" src="images/icon_peche-mangue.png" alt="">
		    	  				<strong class="primary-color">P&#234;che-Mangue</strong>
	    	  				</p>
	    	  			</div>
		    	  		<div class="col-xs-4 col-md-4">
		    	  			<p class="text-center">
		    	  				<img width="100%" src="images/icon_mocka.png" alt="">
		    	  				<strong class="primary-color">Moka</strong>
	    	  				</p>
		    	  		</div>
		    	  		<div class="col-xs-4 col-md-4">
		    	  			<p class="text-center">
		    	  				<img width="100%" src="images/icon_fraise.png" alt="">
		    	  				<strong class="primary-color">Fraise</strong>
	    	  				</p>
		    	  		</div>
		    	  		<div class="col-xs-4 col-md-4">
		    	  			<p class="text-center">
		    	  				<img width="100%" src="images/icon_fruit-rouge.png" alt="">
		    	  				<strong class="primary-color">Fruits rouges</strong>
	    	  				</p>
		    	  		</div>
	    	  		</div>
	    	  		<button data-featherlight="#popinObservance" class="primary-color mt3 mb3 gtmClickPopin" data-gtm="observance-popin">Que pensent les patients de Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml&nbsp;?</button>
	    	  	</div>
	  		</div>
	  	</div>
  	</div>
  	<div class="absolutePosition bottom left abbreviation secondary-color">
	<p class="small mb0">
	CNO : compl&#233;ment nutritionnel oral<br>
	HPHE : hyperprotidique hyper&#233;nerg&#233;tique</p>
	</div>	
	<div data-menuanchor="posologie" class="absolutePosition bottom center nextPage secondary-color text-center">
		<a class="secondary-color text-uppercase gtmClickNavigate" data-gtm="nextPage-nav" href="#posologie">COMMENT PRESCRIRE<br>Fortimel<sup>&#174;</sup>Protein 200&nbsp;ml&nbsp;?</a>
	</div>
	<div class="absolutePosition bottom right reference secondary-color">
		<a class="secondary-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>
</section>