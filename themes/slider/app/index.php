<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Arcoxia</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">


        <!--[if lte IE 9]>
            <script src="../../../_player/js/vendor/html5shiv.js"></script>
        <![endif]-->
    </head> 
    <body>

        <div class="reveal">
            
            <div class="slides">
               
                <?php

                    function recursiveDirectoryInclude($dir) {
                        $cdir = scandir($dir);
                        foreach ($cdir as $key => $value) {
                         if (!in_array($value, array('.', '..', '.DS_Store', '._.DS_Store'))) {
                          if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                            recursiveDirectoryInclude($dir . DIRECTORY_SEPARATOR . $value);
                          }
                          else {
                              include $dir . DIRECTORY_SEPARATOR . $value;
                          }
                         }
                        }
                    }

                    recursiveDirectoryInclude('inc');
                ?>
            </div>

        </div>

                
        <script>

        </script>

    </body>
</html>
