<section id="conclusion_10" class="section fp-auto-height-responsive white-color" data-screen-id="2017_FOR_ED1_S009" data-screen-label="Conclusion" data-screen-name="Conclusion" data-screen-section="">
	<div class="slide fp-auto-height">
    	<div class="container">
    		<div class="row">
    			<div class="col-xs-12 col-md-8 col-md-offset-2">
    				<p class="text-center mb1">
    					<img class="hidden-xs" src="images/img_conclusion.png" alt="">
    					<img class="mainImage visible-xs" src="images/mainImage_responsive.png" alt="">
    				</p>
			    	<p class="white-color"><strong>Ne laissez pas la d&#233;nutrition affecter la vie et les activit&#233;s quotidiennes de vos patients &#226;g&#233;s. </strong></p>
			    	<p class="white-color">Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml est le seul CNO HPHE concentr&#233; &#224; l&#8217;efficacit&#233; cliniquement prouv&#233;e pour aider les patients d&#233;nutris ou &#224; risque de d&#233;nutrition &#224; regagner du poids et de la force<sup>2</sup>.  
					</p>
			    	<p class="white-color">
			    		Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml est disponible en une large vari&#233;t&#233; d&#8217;ar&#244;mes savoureux pour satisfaire les patients et favoriser l&#8217;observance.
					</p>
			    	<p class="white-color">
				    	<strong>
				    		Faites confiance &#224; un CNO qui a fait ses preuves et que les patients appr&#233;cient<sup>2</sup> <br>&#8211; Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml
				    	</strong>
			    	</p>
			    	<center class="mt3">
				    	<a href="http://www.nutricia.fr/nos-produits/nutrition-orale-adulte/fortimel-protein-200ml/?utm_source=Univadis&utm_medium=Banner&utm_campaign=MMM2017" class="button" target="_blank">
				    		En savoir plus sur Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml 
				    	</a>
			    	</center>
		    	</div>
	    	</div>
    	</div>
	</div>
	<div class="absolutePosition bottom left abbreviation secondary-color">
	<p class="small white-color mb0">
	CNO : compl&#233;ment nutritionnel oral<br>
	HPHE : hyperprotidique hyper&#233;nerg&#233;tique</p>
	</div>	
	<div class="absolutePosition bottom right reference white-color">
		<a class="white-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>	
</section>