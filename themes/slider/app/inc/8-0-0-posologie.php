<section id="posologie_09" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S008" data-screen-label="Posologie" data-screen-name="Posologie" data-screen-section="">
	<div class="slide fp-auto-height">
    	
	  	<div class="container">
	  		<h2>
	  			Comment prescrire
    	  	</h2>
    	  	<h3 class="text-uppercase">
	    	  	FORTIMEL<sup>&#174;</sup>  PROTEIN 200&nbsp;ML&nbsp;?
    	  	</h3>
    	  	<div class="row mb2">
    	  		<div class="col-xs-12 col-md-6">
    	  			<div class="titleGamme">
			    		<center>
			    			<h4>Posologie</h4>
			    		</center>
		    		</div>
					<p class="text-center">
						L&#8217;alimentation de votre patient couvre-t-elle ses besoins en prot&#233;ines (1,2 &#224; 1,5 g de prot&#233;ine/kg/jour)&nbsp;?
					</p>
    	  		</div>
    	  		<div class="col-xs-12 col-md-6 hidden-xs hidden-sm">
    	  			<div class="titleGamme">
			    		<center>
			    			<h4 class="blue">Modalit&#233;s de prise en charge</h4>
			    		</center>
		    		</div>
    	  		</div>
    	  	</div>
    	  	<div class="row">
    	  		<div class="col-xs-12 col-md-6 borderRight">
					<div class="col-xs-12 col-sm-6 col-md-6 mb2">
						<p class="text-center"><img width="157" src="images/icon_posologie-1bottle.png" alt=""></p>
						<p>
							<strong class="secondary-color">
								<span class="primary-color">Plus de 50&nbsp;% de ses besoins&nbsp;:</span><br> 1 bouteille/jour = 29&nbsp;g de prot&#233;ines
							</strong>
						</p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 mb2">
						<p class="text-center"><img width="157" src="images/icon_posologie-2bottles.png" alt=""></p>
						<p>
							<strong class="secondary-color">
								<span class="primary-color">Moins de 50&nbsp;% de ses besoins&nbsp;:</span><br> 1 &#224; 2 bouteilles/jour = 29 &#224; 58&nbsp;g de prot&#233;ines
							</strong>
						</p>
					</div>
    	  		</div>
    	  		<div class="col-xs-12 col-md-6 visible-xs visible-sm">
	    	  			<div class="titleGamme">
				    		<center>
				    			<h4 class="blue">Modalit&#233;s de prise en charge</h4>
				    		</center>
			    		</div>
	    	  		</div>
    	  		<div class="col-xs-12 col-md-6">
					<div class="col-xs-12 col-sm-6 col-md-6 mb2">
						<p class="text-center"><img width="157" src="images/icon_posologie-60.png" alt=""></p>
						<p>
							<strong class="secondary-color">
								 Fortimel<sup>&#174;</sup>Protein 200&nbsp;ml est pris en charge au titre de la LPP <span class="primary-color">en cas de d&#233;nutrition (60&nbsp;% du prix LPP)<sup>3</sup>.</span>
							</strong>
						</p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 mb2">
						<p class="text-center"><img width="157" src="images/icon_posologie-100.png" alt=""></p>
						<p>
							<strong class="secondary-color">
								 Si la d&#233;nutrition est li&#233;e &#224; une ALD, la prise en charge de <span class="primary-color">Fortimel<sup>&#174;</sup> Protein 200&nbsp;ml est de 100&nbsp;%.</span>
							</strong>
						</p>
					</div>
    	  		</div>
    	  	</div>
    	  	<div class="row mt1">
    	  		<div class="col-xs-12 col-sm-3">
    	  			<p class="bubbled secondary-color pull-right"><strong>LPP: 1141330<sup>4</sup></strong></p>
    	  		</div>
    	  		<div class="col-xs-12 col-sm-8">
    	  			<p class="secondary-color text-left"><strong>Pour que vos patients retirent tous les b&#233;n&#233;fices de leur traitement nutritionnel, pensez &#224; pr&#233;ciser &#171;Fortimel<sup>&#174;</sup> Protein 200 ml&#187; et &#171;LPP 1141330&#187; sur l'ordonnance.</strong></p>
    	  		</div>
    	  	</div>
	  	</div>
  	</div>
  	<div class="absolutePosition bottom left abbreviation secondary-color">
		<p class="small mb0">
		ALD : affection de longue dur&#233;e <br> 
		CNO : compl&#233;ment nutritionnel oral <br>
		LPP : liste des produits et des prestations</p>
	</div>	
	<div data-menuanchor="end" class="absolutePosition bottom center nextPage secondary-color text-center">
		<a class="secondary-color text-uppercase gtmClickNavigate" data-gtm="nextPage-nav"href="#end">INTERESS&#201; PAR<br>FORTIMEL<sup>&#174;</sup>  PROTEIN 200&nbsp;ML&nbsp;?</a>
	</div>
	<div class="absolutePosition bottom right reference secondary-color">
		<a class="secondary-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>
</section>