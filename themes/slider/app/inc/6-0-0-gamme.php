<section id="gamme_07" class="section fp-auto-height-responsive" data-screen-id="2017_FOR_ED1_S006" data-screen-label="Gamme" data-screen-name="Gamme" data-screen-section="">
	<div id="slideGamme0" class="slide" data-anchor="1">
		<h2>
  		Les essentiels
	  	</h2>
	  	<h3 class="text-uppercase">
    	  	 de la gamme Fortimel<sup>&#174;</sup>
	  	</h3> 
		<div class="container">
	  		<div class="row">
	    		<div class="titleGamme">
		    		<center>
		    			<h4>La r&#233;f&#233;rence 200&nbsp;ml</h4>
		    		</center>
	    		</div>
	    		<center>
	    		<div class="imgProducts col7">
	    			<div class="product">
						<img width="138" src="images/reference200ml-caramel.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/reference200ml-mocka.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/reference200ml-banane.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/reference200ml-fruit_rouge.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/reference200ml-peche_mangue.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/reference200ml-vanille.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/reference200ml-fraise.png" alt="">
					</div>
	    		</div>
	    		</center>
    		</div>
    		<div class="row mt2">
    			<div class="col-xs-0 col-md-1">
    			</div>
    			<div class="col-xs-12 col-sm-5 col-md-5">
    				<div class="col-xs-0 col-sm-1 col-md-1"></div>
    				<div class="col-xs-6 col-sm-4 col-md-4">
						<img src="images/logo-protein200.png" alt="">
    				</div>
    				<div class="col-xs-6 col-md-2 visible-xs text-right">
	    				<img width="90" src="images/nutrition_logo1.png" alt="">
	    			</div>
    				<div class="col-xs-12 col-sm-5 col-md-5 ">
    					<p class="mt1"><strong class="primary-color">Le plus concentr&#233; en prot&#233;ines et &#233;nergie</strong></p>
    				</div>
    				<div class="col-xs-12 col-sm-1 col-md-1">
    				</div>
    			</div>
    			
    			<div class="col-xs-12 col-sm-4 col-md-4">
    				<p class="mt1 pl-xs">
    					<strong class="secondary-color">Posologie : 1 &#224; 2 par jour<br>
    					Cat&#233;gorie : HPHE concentr&#233; </strong><br>
    					Une concentration in&#233;gal&#233;e en prot&#233;ines et en &#233;nergie, par rapport aux autres CNO de la gamme.<br>
    					Disponible en 7 ar&ocirc;mes: caramel, moka, banane, fruits rouges, p&ecirc;che-mangue, vanille, fraise		   
    				</p>
    			</div>
    			<div class="col-xs-12 col-sm-2 col-md-2 hidden-xs">
    				<img width="90" src="images/nutrition_logo1.png" alt="">
    			</div>
    		</div>
		</div>
	</div>
	<div id="slideGamme1" class="slide" data-anchor="2">
		<h2>
  		Les essentiels
	  	</h2>
	  	<h3 class="text-uppercase">
    	  	 de la gamme Fortimel<sup>&#174;</sup>
	  	</h3> 
		<div class="container">
	  		<div class="row">
	    		<div class="titleGamme">
		    		<center>
		    			<h4 class="blue">Vari&#233;t&#233; et plaisir</h4>
		    		</center>
	    		</div>
	    		<center>
	    		<div class="imgProducts col5">
					<div class="product">
						<img width="138" src="images/variete-plaisir-1-1.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-1-2.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-1-3.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-1-4.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-1-5.png" alt="">
					</div>
	    		</div>
	    		</center>
    		</div>
    		<div class="row mt2">
    			<div class="col-xs-0 col-md-1">
    			</div>
    			<div class="col-xs-12 col-sm-5 col-md-5">
    				<div class="col-xs-0 col-sm-2 col-md-2"></div>
    				<div class="col-xs-6 col-sm-4 col-md-4 pr0">
						<img src="images/logo-creme.png" alt="">
    				</div>
    				<div class="col-xs-6 col-md-2 visible-xs text-right">
	    				<img width="90" src="images/nutrition_logo2.png" alt="">
	    			</div>
    				<div class="col-xs-12 col-sm-5 col-md-5 pr0">
    					<p class="mt1"><strong class="primary-color">Cr&#232;me onctueuse</strong></p>
    				</div>
    			</div>
    			
    			<div class="col-xs-12 col-sm-4 col-md-4">
    				<p class="mt1 pl-xs">
    					<strong class="secondary-color">Posologie : 1 &#224; 3 par jour<br>
    					Cat&#233;gorie : HPHE </strong><br>
    					Une cr&#232;me onctueuse qui peut &#234;tre consomm&#233;e en glace. Peut convenir en cas de troubles de la d&#233;glutition.<br>
    					Disponible en 5 ar&ocirc;mes: banane, chocolat, fruits de la for&ecirc;t, moka, vanille		   
    				</p>
    			</div>
    			<div class="col-xs-4 col-sm-2 col-md-2 hidden-xs">
    				<img width="90" src="images/nutrition_logo2.png" alt="">
    			</div>
    		</div>
		</div>
	</div>
	<div id="slideGamme2" class="slide" data-anchor="3">
		<h2>
  		Les essentiels
	  	</h2>
	  	<h3 class="text-uppercase">
    	  	 de la gamme Fortimel<sup>&#174;</sup>
	  	</h3> 
		<div class="container">
	  		<div class="row">
	    		<div class="titleGamme">
		    		<center>
		    			<h4 class="blue">Vari&#233;t&#233; et plaisir</h4>
		    		</center>
	    		</div>
	    		<center>
	    		<div class="imgProducts col6">
					<div class="product">
						<img width="138" src="images/variete-plaisir-2-1.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-2-2.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-2-3.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-2-4.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-2-5.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-2-6.png" alt="">
					</div>
	    		</div>
	    		</center>
    		</div>
    		<div class="row mt2">
    			<div class="col-xs-0 col-md-1">
    			</div>
    			<div class="col-xs-12 col-sm-5 col-md-5">
    				<div class="col-xs-0 col-sm-2 col-md-2"></div>
    				<div class="col-xs-6 col-sm-4 col-md-4 pr0">
						<img src="images/logo-jucy.png" alt="">
    				</div>
    				<div class="col-xs-6 col-md-2 visible-xs text-right">
	    				<img width="90" src="images/nutrition_logo3.png" alt="">
	    			</div>
    				<div class="col-xs-12 col-sm-5 col-md-5 pr0">
    					<p class="mt1"><strong class="primary-color">Jus</strong></p>
    				</div>
    				<div class="col-xs-0 col-sm-1 col-md-1"></div>
    			</div>
    			
    			<div class="col-xs-12 col-sm-4 col-md-4">
    				<p class="mt1 pl-xs">
    					<strong class="secondary-color">Posologie : 1 &#224; 4 par jour<br>
    					Cat&#233;gorie : HE</strong><br>
    					Une texture &#171; jus &#187;. &#192; consommer tr&#232;s frais.<br>
    					Disponible en 6 ar&ocirc;mes: fraise, fruits de la for&ecirc;t, orange, pomme, tropical, cassis
    				</p>
    			</div>
    			<div class="col-xs-4 col-sm-2 col-md-2 hidden-xs">
    				<img width="90" src="images/nutrition_logo3.png" alt="">
    			</div>
    		</div>
		</div>
	</div>
	<div id="slideGamme3" class="slide" data-anchor="4">
		<h2>
  		Les essentiels
	  	</h2>
	  	<h3 class="text-uppercase">
    	  	 de la gamme Fortimel<sup>&#174;</sup>
	  	</h3> 
		<div class="container">
	  		<div class="row">
	    		<div class="titleGamme">
		    		<center>
		    			<h4 class="blue">Vari&#233;t&#233; et plaisir</h4>
		    		</center>
	    		</div>
	    		<center>
	    		<div class="imgProducts col7">
					<div class="product">
						<img width="138" src="images/variete-plaisir-3-1.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-3-2.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-3-3.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-3-4.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-3-5.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-3-6.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/variete-plaisir-3-7.png" alt="">
					</div>
	    		</div>
	    		</center>
    		</div>
    		<div class="row mt2">
    			<div class="col-xs-0 col-sm-1 col-md-1">
    			</div>
    			<div class="col-xs-12 col-sm-5 col-md-5">
    				<div class="col-xs-6 col-sm-4 col-md-4 pr0">
						<img class="mt2" src="images/logo-extra.png" alt="">
    				</div>
    				<div class="col-xs-6 col-sm-2 col-md-2 text-right visible-xs">
	    				<img width="90" src="images/nutrition_logo4.png" alt="">
	    			</div>
    				<div class="col-xs-10 col-sm-5 col-md-5 pr0">
    					<p class="mt3"><strong class="primary-color">Texture fluide</strong></p>
    				</div>
    				<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0">
    					&nbsp;
    				</div>
    			</div>
    			
    			<div class="col-xs-12 col-sm-4 col-md-4">
    				<p class="pl-xs">
    					<strong class="secondary-color">Posologie : 1 &#224; 3 par jour<br>
    					Cat&#233;gorie : HPHE</strong><br>
    					Le plus grand choix d'ar&ocirc;mes.<br>
    					Disponible en 7 ar&ocirc;mes: abricot, fraise, fruits de la for&ecirc;t, moka, neutre, vanille, chocolat	 
    				</p>
    			</div>
    			<div class="col-xs-4 col-sm-2 col-md-2 hidden-xs">
    				<img width="90" src="images/nutrition_logo4.png" alt="">
    			</div>
    		</div>
		</div>
	</div>
	<div id="slideGamme4" class="slide" data-anchor="5">
		<h2>
  		Les essentiels
	  	</h2>
	  	<h3 class="text-uppercase">
    	  	 de la gamme Fortimel<sup>&#174;</sup>
	  	</h3> 
		<div class="container">
	  		<div class="row">
	    		<div class="titleGamme">
		    		<center>
		    			<h4 class="orange">300 ml</h4>
		    		</center>
	    		</div>
	    		<center>
	    		<div class="imgProducts col4">
					<div class="product">
						<img width="138" src="images/gamme300-1.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/gamme300-2.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/gamme300-3.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/gamme300-4.png" alt="">
					</div>
	    		</div>
	    		</center>
    		</div>
    		<div class="row mt2">
    			<div class="col-xs-0 col-sm-1 col-md-1">
    			</div>
    			<div class="col-xs-12 col-sm-5 col-md-5">
    				<div class="col-xs-0 col-sm-2 col-md-2"></div>
    				<div class="col-xs-6 col-sm-4 col-md-4 pr0">
						<img src="images/logo-max.png" alt="">
    				</div>
    				<div class="col-xs-6 col-md-2 visible-xs text-right">
	    				<img width="90" src="images/nutrition_logo5.png" alt="">
	    			</div>
    				<div class="col-xs-12 col-sm-4 col-md-4 pr0">
    					<p class="mt1"><strong class="primary-color">Format 1/jour</strong></p>
    				</div>
    			</div>
    			
    			<div class="col-xs-12 col-sm-4 col-md-4">
    				<p class="mt1 pl-xs">
    					<strong class="secondary-color">Posologie : 1 par jour<br>
						Cat&#233;gorie : HPHE 
						</strong><br>
						Disponible en 4 ar&ocirc;mes: chocolat, fraise, vanille, caf&eacute;
    				</p>
    			</div>
    			<div class="col-xs-4 col-sm-2 col-md-2 hidden-xs">
    				<img width="90" src="images/nutrition_logo5.png" alt="">
    			</div>
    		</div>
		</div>
	</div>
	<div id="slideGamme5" class="slide" data-anchor="6">
		<h2>
  		Les essentiels
	  	</h2>
	  	<h3 class="text-uppercase">
    	  	 de la gamme Fortimel<sup>&#174;</sup>
	  	</h3> 
		<div class="container">
	  		<div class="row">
	    		<div class="titleGamme">
		    		<center>
		    			<h4 class="green">D&#233;nutrition et diab&#232;te</h4>
		    		</center>
	    		</div>
	    		<center>
	    		<div class="imgProducts col3">
					<div class="product">
						<img width="138" src="images/besoins-1-1.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/besoins-1-2.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/besoins-1-3.png" alt="">
					</div>
	    		</div>
	    		</center>
    		</div>
    		<div class="row mt2">
    			<div class="col-xs-0 col-sm-1 col-md-1">
    			</div>
    			<div class="col-xs-12 col-sm-5 col-md-4">
    				<div class="col-xs-6 col-sm-4 col-md-5 pr0">
						<img class="mt2" src="images/logo-diacare.png" alt="">
    				</div>
    				<div class="col-xs-6 col-sm-2 col-md-2 text-right visible-xs">
	    				<img width="90" src="images/nutrition_logo6.png" alt="">
	    			</div>
    				<div class="col-xs-10 col-sm-5 col-md-5 pr0">
    					&nbsp;
    				</div>
    				<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0">
    					<img class="mt2" src="images/logo-lowGi.png" alt="">
    				</div>
    			</div>
    			
    			<div class="col-xs-12 col-sm-4 col-md-5">
    				<p class="pl-xs">
    					<strong class="secondary-color">Posologie : 1 &#224; 2 par jour<br>
    					Cat&#233;gorie : HPHE</strong><br>
    					Sans saccharose. Contient de l&#8217;isomaltulose et des &#233;dulcorants (sans aspartame).  Pour les conseils d&#8217;utilisation et pr&#233;cautions d&#8217;usage, merci de vous r&#233;f&#233;rer &#224; l&#8217;emballage du produit.<br>
    					Disponible en 3 ar&ocirc;mes: chocolat, vanille, fraise	  
    				</p>
    			</div>
    			<div class="col-xs-4 col-sm-2 col-md-2 hidden-xs">
    				<img width="90" src="images/nutrition_logo6.png" alt="">
    			</div>
    		</div>
		</div>
	</div>
	<div id="slideGamme6" class="slide" data-anchor="7">
		<h2>
  		Les essentiels
	  	</h2>
	  	<h3 class="text-uppercase">
    	  	 de la gamme Fortimel<sup>&#174;</sup>
	  	</h3> 
		<div class="container">
	  		<div class="row">
	    		<div class="titleGamme">
		    		<center>
		    			<h4 class="green">D&#233;nutrition et diab&#232;te</h4>
		    		</center>
	    		</div>
	    		<center>
	    		<div class="imgProducts col2">
					<div class="product">
						<img width="138" src="images/besoins-2-1.png" alt="">
					</div>
					<div class="product">
						<img width="138" src="images/besoins-2-2.png" alt="">
					</div>
	    		</div>
	    		</center>
    		</div>
    		<div class="row mt2">
    			<div class="col-xs-0 col-sm-1 col-md-1">
    			</div>
    			<div class="col-xs-12 col-sm-5 col-md-4">
    				<div class="col-xs-6 col-sm-4 col-md-5 pr0">
						<img class="mt2" src="images/logo-diacare-creme.png" alt="">
    				</div>
    				<div class="col-xs-6 col-sm-2 col-md-2 text-right visible-xs">
	    				<img width="90" src="images/nutrition_logo7.png" alt="">
	    			</div>
    				<div class="col-xs-10 col-sm-5 col-md-5 pr0">
    					&nbsp;
    				</div>
    				<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0">
    					<img class="mt2" src="images/logo-lowGi.png" alt="">
    				</div>
    			</div>
    			
    			<div class="col-xs-12 col-sm-4 col-md-5">
    				<p class="pl-xs">
    					<strong class="secondary-color">Posologie : 1 &#224; 2 par jour<br>
    					Cat&#233;gorie : HPHE</strong><br>
    					Sans saccharose. Contient de l&#8217;isomaltulose et des &#233;dulcorants (sans aspartame). Peut convenir en cas de troubles de la d&#233;glutition.  Pour les conseils d&#8217;utilisation et pr&#233;cautions d&#8217;usage, merci de vous r&#233;f&#233;rer &#224; l&#8217;emballage du produit.
    					<br>
    					Disponible en 2 ar&ocirc;mes: chocolat, vanille	  
    				</p>
    			</div>
    			<div class="col-xs-4 col-sm-2 col-md-2 hidden-xs">
    				<img width="90" src="images/nutrition_logo7.png" alt="">
    			</div>
    		</div>
		</div>
	</div>
	
	<div class="absolutePosition bottom left abbreviation secondary-color">
	<p class="small mb0">
	CNO : compl&#233;ment nutritionnel oral<br>
	HE : hyper&#233;nerg&#233;tique <br>
	HPHE : hyperprotidique hyper&#233;nerg&#233;tique</p>
	</div>	
	<div data-menuanchor="observance" class="absolutePosition bottom center nextPage secondary-color text-center">
		<a class="secondary-color text-uppercase gtmClickNavigate" data-gtm="nextPage-nav" href="#observance">FAVORISER L&#8217;OBSERVANCE<br>DU TRAITEMENT</a>
	</div>
	<div class="absolutePosition bottom right reference secondary-color">
		<a class="secondary-color text-uppercase gtmClickPopin" data-gtm="references-popin" href="#" data-featherlight="#popinReferences">R&#233;f&#233;rences</a>
	</div>
</section>