function gsapAnimation (origin, target, duration, animationType, delayTime, staggerTime){
        if(!delayTime){ delayTime = 0; }
        if(!staggerTime){ staggerTime = 0; }
        
        var vars;
        switch(animationType) {
            case "fadeIn":
                vars = {opacity:0, delay:delayTime}
                break;
            case "fadeOut":
                vars = {opacity:1, delay:delayTime}
                break;
            case "slideFromLeft":
                vars = {opacity:0, x:-1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromRight":
                vars = {opacity:0, x:1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromTop":
                vars = {opacity:0, y:-1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromMiddleTop":
                vars = {opacity:0, y:-50, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromBottom":
                vars = {opacity:0, y:1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "zoomIn":
                vars = {opacity:0, scale:0, ease:Power2.easeOut, delay:delayTime}
                break;
            case "zoomInOut":
                vars = {opacity:0, scale:0, ease:Back.easeInOut.config(1.7), delay:delayTime}
                break;
            case "bounce":
                vars = {scale:0, ease: Bounce.easeOut, delay:delayTime}
                break;
            case "rotation":
                vars = {rotation:360, repeat:1, ease:Power0.easeNone, delay:delayTime}
                break;
            case "infiniteRotation":
                vars = {rotation:360, repeat:-1, ease:Power0.easeNone, delay:delayTime}
                break;
            case "slowWiggle":
                vars = {scale:0.95, yoyo:true, repeat:-1, ease:Bounce.easeInOut, delay:delayTime}
                break;
            case "borderWhite":
                vars = {css:{borderRight:'2px solid rgba(255, 255, 255, 0)'},ease:Power2.easeOut, delay:delayTime}
                break;
            case "borderBlack":
                vars = {css:{borderRight:'2px solid rgba(255, 255, 255, 0)'},ease:Power2.easeOut, delay:delayTime}
                break;
            case "zoomJames":
                vars = {scale:1.3, top:'370px', left:'-40%', ease:Power2.easeOut, delay:delayTime}
                break;
        }
        switch(origin) {
            case "to":
                TweenMax.to(target, duration, vars);
                break;
            case "from":
                TweenMax.from(target, duration, vars);
                break;
            case "staggerFrom":
                TweenMax.staggerFrom(target, duration, vars, staggerTime);
                break;
            case "staggerTo":
                TweenMax.staggerTo(target, duration, vars, staggerTime);
                break;
        }
        
    }

(function ($) {
    
    

    function profileAnimation(currentSlide){
         gsapAnimation('to', currentSlide.find('.imgProfil'), 1.5, 'zoomJames', 0);
         gsapAnimation('staggerFrom', currentSlide.find('.bubbleContent'), 1.5, 'zoomIn', 0.5, 0.5);
         gsapAnimation('staggerFrom', currentSlide.find('h2, .characteristic, ul li'), 1, 'fadeIn', 1, 0.5);
    }
    function homeAnimation(currentSlide){
        if(currentSlide.hasClass('present')){


            gsapAnimation('staggerFrom', currentSlide.find('.imgBubble, .imgProduct, .whiteBubble'), 2, 'zoomIn', 0);
            gsapAnimation('from', currentSlide.find('.slideContent h2, .slideContent .introP, button'), 2, 'fadeIn', 1);
            
            gsapAnimation('from', currentSlide.find('.profil.james'), 2, 'slideFromLeft', 1);
            gsapAnimation('from', currentSlide.find('.imgJames'), 2, 'slideFromLeft', 1.5);
            
            gsapAnimation('from', currentSlide.find('.profil.karen'), 2, 'slideFromRight', 1);
            gsapAnimation('from', currentSlide.find('.imgKaren'), 2, 'slideFromRight', 1.5);
            

            gsapAnimation('from', currentSlide.find('.bottomHome'), 2, 'slideFromBottom', 2);
        }
    }
    
    
})(jQuery);