jQuery(document).ready(function() {

  //HEADER & NAV
  jQuery(window).on('sectionChange', function (e) {
      var currentPopin = jQuery('section.section.active').attr('id');
      if(currentPopin == 'home_01'){
        jQuery('header').addClass('big');
      }else{
        jQuery('header').removeClass('big');
      }
    });

  jQuery('header nav ul.nav li a').on('click touchend', function(e){
      if(jQuery('body').hasClass('fp-responsive')){
        jQuery('.navmenu').offcanvas('toggle');

        /***Hot fix -  Scrolling issue***/
        setTimeout(function(){
          jQuery('body').trigger( "click" );
        }, 1000);

      } 
  });

  jQuery('.navbar-toggle').on('click touchend', function(e){
      if(jQuery('body').hasClass('fp-responsive')){
        jQuery('.navmenu').offcanvas('toggle');
        return false;
      } 
  });


  
  jQuery('p.quizAnswer').hide();
  /***** QUIZ - TESTEZ-VOUS ******/
  jQuery('#quiz_02 button').on('click touch',
  		function(){
  			var target = jQuery(this).data('target'), 
  				tl = new TimelineLite();
  			tl.to(jQuery('#allAnswers'), 0.5, {opacity:0, x:-1000, ease:Power2.easeOut, onComplete:function(){
  				jQuery('#allAnswers').hide();
  				jQuery('#showAnwserContainer').css('min-height',jQuery('#allAnswers').height()).show();
  				jQuery('#answer'+target).css('transform', '').show();
  				jQuery('#footerAnswer').show();
  			}})
  				.to(jQuery('#answer'+target), 0.5, {opacity:1, right:0, ease:Power2.easeOut})
  				.to(jQuery('#footerAnswer'), 1, {opacity:1, right:0, ease:Power2.easeOut});
  		}
  );

  jQuery('#quiz_02 .retryQuiz').on('click touch',
      function(){
        var tl = new TimelineLite();
        tl.to(jQuery('.showAnwser, #footerAnswer'), 0.5, {opacity:0, right:1000, ease:Power2.easeOut, onComplete:function(){
          jQuery('#allAnswers').show();
          jQuery('#showAnwserContainer').hide();
          jQuery('.showAnwser').hide();
          jQuery('#footerAnswer').hide();
        }})
          .to(jQuery('#allAnswers'), 0.5, {opacity:1, x:0, ease:Power2.easeOut});

          return false;
      }
  );

  /***** QUIZ - POPIN ******/
  jQuery('#popinQuiz input').on('click touch',
	  	function(){
        var currentPopin = $.featherlight.current(),
            formId = jQuery(this)[0].form.id,
            isCorrect = jQuery(this).data('result'),
            quizAnswer = jQuery(this).parentsUntil('form').parent().next('.quizAnswer');

        jQuery(this).parentsUntil('form').addClass('showAnswer');

        //Show answer Right/Wrong answer
        if(isCorrect === 'correct'){
          quizAnswer.children('span').append('Réponse correcte !').addClass('rightAnswer');
        } else {
          quizAnswer.children('span').append('Mauvaise réponse !').addClass('wrongAnswer');
        }
        quizAnswer.show();

        jQuery('#'+formId+' input[type=radio]').attr("disabled",true);

        var count = 0;
        jQuery('.featherlight #popinQuiz input[type=radio]').each(function(){
          if(jQuery(this).attr("disabled")){
            count++
          }
        });

        if(count == 6){
          jQuery('.featherlight #popinQuiz .closeButton').css('visibility', 'visible');
        }

	  	}

	);

  //Re-init quiz
  jQuery('#diagnostic_04 button').on('click', function(){
      jQuery('#popinQuiz input[type=radio]').removeAttr("disabled");
  })

   /******* POPIN - FEATHERLIGHT ********/

   jQuery('[data-featherlight]').on('click touch', function(){
      var isResponsive = jQuery('.fp-responsive').length;
      if(isResponsive == 1){
        jQuery('body').css('overflow', 'hidden');
      }
    
   });

   jQuery(window).on('mousewheel touchmove DOMMouseScroll', function(){
     	var currentPopin = $.featherlight.current();
      var isResponsive = jQuery('.fp-responsive').length;
    	if(currentPopin && isResponsive == 0){
    		currentPopin.close();
    	}
   });

   jQuery('.nextPage, .closeButton, .closePopin').on('click touch',
     	function(){
     		var currentPopin = $.featherlight.current();
        var isResponsive = jQuery('.fp-responsive').length;
     		
    		if(currentPopin){
    			currentPopin.close();
    		}

        if(isResponsive == 1){ 
          jQuery('body').css('overflow', 'auto');
        }
     	}
   );

});