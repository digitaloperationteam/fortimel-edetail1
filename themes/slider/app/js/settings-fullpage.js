jQuery(document).ready(function() {

	/********************
		ACTIVATION KEY
	********************/
	var activationkey = {
    	scrollOverflowReset : {
    		uat:'YWNxdWlhLXNpdGVzLmNvbV8xcE1jMk55YjJ4c1QzWmxjbVpzYjNkU1pYTmxkQT09RzZX',
    		prod: 'ZS1kZXRhaWxpbmcuY29tX1NTU2MyTnliMnhzVDNabGNtWnNiM2RTWlhObGRBPT1LMXg='
    	},
    	scrollHorizontally : {
    		uat:'YWNxdWlhLXNpdGVzLmNvbV9BekNjMk55YjJ4c1NHOXlhWHB2Ym5SaGJHeDVPUWI=',
    		prod: 'ZS1kZXRhaWxpbmcuY29tX245WWMyTnliMnhzU0c5eWFYcHZiblJoYkd4NU04ag=='
    	}
    },
    	$scrollHorizontallyKey = '',
    	$scrollOverflowResetKey= '';

    if(window.location.href.indexOf("acquia-sites.com") > -1) {
		$scrollHorizontallyKey 		= 	activationkey.scrollHorizontally.uat,
		$scrollOverflowResetKey 	= 	activationkey.scrollOverflowReset.uat;
    }else if(window.location.href.indexOf("e-detailing.com") > -1){	
		$scrollHorizontallyKey 		= 	activationkey.scrollHorizontally.prod,
		$scrollOverflowResetKey 	= 	activationkey.scrollOverflowReset.prod;
    }
    /*******************************
    ********************************/

    
	if ( $(window).width() < 1024) { 
        var $scroll = false;
        if($(window).width() <= 768)
            {
            	jQuery('.noSlideForResponsive').removeClass('slide');
            	jQuery('.slideForResponsive').addClass('slide');
            }
    } else{
        var $scroll = true;
    }
    createFullpage($scroll);

    var windowWidth = $(window).width();
		var windowHeight = $(window).height();

    $( window ).on('resizeend resize', function() {
    	if(windowWidth != $(window).width() || windowHeight != $(window).height()) {
	      location.reload();
	      return;
	      controlArrowsPos();
	    }
    });

    //Custom event to detect when fullpage is ready
	function fullpageReady(){
		$.event.trigger({
		  type:    "fullpageReady",
		  message: "fullpage is ready"
		});
	}

	//Custom event to detect slide change and retrive its name
	function sectionChange(){
		$.event.trigger({
		  type:    "sectionChange",
		  message: "Page has changed",
		});
	}

	//Custom event to detect slide change and retrive its name
	function slideChange(sectionName){
		$.event.trigger({
		  type:    "slideChange",
		  message: "slide has changed",
		  section: sectionName
		});
	}
   
    function createFullpage($scroll){
    	jQuery('#fullpage').fullpage({
    		bigSectionsDestination:'top',
	    	scrollHorizontally: true,
		    scrollHorizontallyKey: $scrollHorizontallyKey,
		    scrollOverflow:$scroll,
		    scrollOverflowReset: $scroll, 
		    scrollOverflowResetKey: $scrollOverflowResetKey,
	    	anchors:['accueil', 'quiz', 'situations-a-risque', 'diagnostic', 'efficacite-clinique','produit','gamme','observance','posologie','end'],
	    	sectionsColor: ['#491a82', '#efecf4', '#ffffff', '#efecf4','#ffffff', '#491a82', '#ffffff', '#efecf4', '#ffffff', '#491a82'],
	    	css3: true,
	    	paddingTop: '6rem',
	    	paddingBottom:'6rem',
	    	fixedElements:'#hearder',
	    	menu: '#menu',
	    	responsiveWidth:1024,
	    	responsiveSlides:false,
	    	navigation: true,
	    	navigationPosition: 'left',
	    	slidesNavigation: true,
			slidesNavPosition: 'bottom',
	    	navigationTooltips: ['Accueil', 'Quiz', 'Situations à risque', 'Diagnostic', 'Efficacité clinique','Produit','Gamme','Observance','Posologie','Conclusion'],
	    	lazyLoading: true,
	    	onLeave: function(index, nextIndex, direction){
	    		if(nextIndex == 7){
	    			gsapAnimation('from', jQuery('#slideGamme0').find('h4'), 1.5, 'slideFromRight', 0.2, 0.1);
	    			gsapAnimation('staggerFrom', jQuery('#slideGamme0').find('.product'), 1, 'slideFromRight', 0.2, 0.1);
	    		}
	    		
	    	},
	    	onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){
	    		if(direction == 'right'){
	    			animationType = 'slideFromRight';
	    			gsapAnimation('from', jQuery('#slideGamme'+nextSlideIndex).find('h4'), 1.5, animationType, 0.2, 0.1);
	    			gsapAnimation('staggerFrom', jQuery('#slideGamme'+nextSlideIndex).find('.product'), 1, animationType, 0.2, 0.1);
	    		}else{
	    			animationType = 'slideFromLeft';
	    		}	
	    	},
	    	afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){
		    	slideChange(anchorLink + "-" + slideAnchor);
	   		},
    		afterLoad: function(anchorLink, index){
    			sectionChange();
    		},
    		afterRender: fullpageReady
    		
	    });
    }

    //Change position of dots navigation
    jQuery('section').each(function(){
    	jQuery(this).find('.fp-slidesNav').appendTo(jQuery(this).find('.fp-slides'));
    }) 

    //Changing position of arrows to folow slide
    let controlArrowsPos = function (){
    	let carouselHeight = jQuery('section#gamme_07 .fp-slides .slide.active .fp-tableCell .container>.row:first-child>center').height();
	    let introHeight = jQuery('section#gamme_07 .fp-slides .slide.active .fp-tableCell h2').height() + jQuery('section#gamme_07 .fp-slides .slide.active .fp-tableCell h3').height() + jQuery('section#gamme_07 .fp-slides .slide.active .fp-tableCell .container>.row:first-child .titleGamme').height();
	    let topSpace = (carouselHeight / 2) + introHeight + 96;
	    jQuery("section#gamme_07 .fp-controlArrow").css({ 
	    	top: topSpace + 'px',
	    	marginTop: '0px'
	    });
    }()
    
    
    
});